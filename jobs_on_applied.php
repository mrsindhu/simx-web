<?php

session_start();
if (!isset($_SESSION["username"])) {
    header("Location:logout.php");
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>SimX</title>
    <link rel="stylesheet" href="style.css" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link href="https://vjs.zencdn.net/7.8.4/video-js.css" rel="stylesheet" />


    <style>
        @import url("//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css");

        .video-js {
            background-color: #fff;
        }

        #candidate_card {
            box-shadow: 10px 10px 5px #DCF3F3;
            border-radius: 10px;
        }

        .avatar {
            vertical-align: middle;
            width: 50px;
            height: 50px;
            border-radius: 50%;
        }

        .for-btnn {
            float: right
        }
    </style>

</head>

<body class="gradient-bg">

    <?php

    $conn = new mysqli("localhost:3307", "root", "Lisaco563", "bitnami_df");
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $userName = $_SESSION["username"];
    $sql = "SELECT * FROM `users` WHERE username = '$userName'";

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $skills = $row['skills'];
            $isClient = ($skills == 'Recruiter') ? true : false;
            if ($isClient) {
                $tabs = '<a href="/applicant_list.php">Applied Jobs</a><a href="/shortlisted_list.php">Shortlisted</a>';
            } else {
                $tabs = '<a href="/jobs_on_applied.php">Applied Jobs</a>';
            }
            echo '
    
    <div id="mySidenav" class="sidenav gradient-bg">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"
        >&times;</a
      >
      <a href="/index.php">Home</a>
      <a href="/myvideos.php">My Videos</a>
      ' . $tabs . '
      <a href="/logout.php">Logout</a>
    </div>

    <div class="container-fluid" style="z-index:11;position:fixed;top:0;background-color:#fff">
      <div class="row pt-3">
        <div class="col-lg-3">
          <div class="logoo">
            <span style="font-size: 30px; cursor: pointer;" onclick="openNav()"
              >&#9776;</span
            >
            <a href="index.php"><img src="simx.png" /></a>
          </div>
          <span id="mobile-logout">
          <span class="pr-3" style="font-weight:bold;">' . $row["name"] . '</span>
          <img
            src="http://www.simx.tv/picture/Photos/' . $row["username"] . '.png"
            alt="Avatar"
            class="avatar"
          />
          </span>
        </div>
        <div class="col-lg-5 pt-2">
          <form class="navbar-form" action="search.php" method="post">
            <div class="input-group">
              <input
                class="form-control"
                type="text"
                name="name"
                placeholder="Search"
              />
              <span class="input-group-btn">
                <button type="submit" name="submit" value="submit" class="btn btn-default">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
            </div>
          </form>
        </div>
        <div id="web-logout" style="text-align: right;" class="col-lg-4 profile-img">
        <span class="pr-3" style="font-weight:bold;">' . $row["name"] . '</span>
          <img
            src="http://www.simx.tv/picture/Photos/' . $row["username"] . '.png"
            alt="Avatar"
            class="avatar"
          />
          <a
            style="background-color: #005585;"
            type="button"
            class="btn btn-info"
            href="logout.php"
          >
            LogOut
          </a>
        </div>
      </div>
    </div>
    ';
        }
    } else {
        echo "0 results";
    }

    $query = "SELECT b.broadcast as broadcast, b.isOffline as isOffline, j.id as id, u.name as uploader_name, u.username as uploader_username, u.link as linkedin, b.imgLink as imgLink, b.title as job_title FROM `jobcandidates` as j, `users` as u, `broadcasts` as b WHERE j.broadcast = b.broadcast AND u.username = b.username AND j.isshortlisted = 0 AND j.username = '$userName'";

    $result = $conn->query($query);

    if ($result->num_rows > 0) {

    ?>


        <div class="container-fluid mt-40" style="background-color: transparent;">
            <h2></h2>
            <div class="row">
                <div class="col-lg-2 col-sm-1"></div>
                <div class="col-lg-8 col-sm-10">
                    <?php
                    while ($row = $result->fetch_assoc()) { // echo "<pre> $query"; print_r($row);
                        if ($row['isOffline']) {
                            $src = "https://simx.s3-us-west-2.amazonaws.com/offlineVideos/$row[broadcast].mp4";
                        } else {
                            $src = "https://simx.s3-us-west-2.amazonaws.com/recordedvideos/$row[broadcast].mp4";
                        }
                    ?>
                        <div class="card mt-4">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-lg-9 col-sm-12">
                                        <?php echo strtoupper($row['job_title']); ?></div>
                                    <div class="col-3 hide text-center">Video Cv</div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-3  col-sm-0">
                                        <img id="applicant_profile" src="<?php echo "http://www.simx.tv/picture/Photos/$row[uploader_username].png"; ?>" alt="" srcset="">
                                        <h5 class="card-title h3"><?php echo $row['uploader_name']; ?></h5>
                                        <!-- <p class="card-text"><small class="text-muted">Hourly Rate: $<?php echo $row['rate']; ?> / hr</small></p>
                                        <p class="card-text">Click to view <a href="https://www.linkedin.com/in/<?php echo $row['linkedin']; ?>">LinkedIn Profile</a></p> -->
                                    </div>
                                    <div class="col-lg-6 col-sm-12">
                                        <p class="text-muted" style="margin-top: 10%;">Your job request will be removed by clicking this . Be sure that you want to widraw you job application</p>
                                        <a class="btn btn-danger vertical-center" href="/remove_apply.php?action=remove&id=<?php echo $row['id']; ?>">Withdraw Application</a>
                                    </div>
                                    <div class="col-lg-3 col-sm-0  p-0">
                                        <video class="video-js" controls width="99%" height="200">
                                            <source src="<?php echo $src; ?>" type="video/mp4" />
                                            <p class="vjs-no-js">
                                                To view this video please enable JavaScript, and consider
                                                upgrading to a web browser that
                                                <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                            </p>
                                        </video>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-lg-2 col-sm-1"></div>
            </div>
        </div>
    <?php } ?>

    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }
    </script>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>