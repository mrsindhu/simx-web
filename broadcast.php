<?php

session_start();
if (!isset($_SESSION["username"])) {
  header("Location:logout.php");
}

?>

<!DOCTYPE html>
<html>

<head>
  <title>SimX</title>
  <link rel="stylesheet" href="style.css" />

  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link href="https://vjs.zencdn.net/7.8.4/video-js.css" rel="stylesheet" />


  <style>
    @import url("//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css");

    .video-js {
      background-image: linear-gradient(to top right,
          rgb(20, 124, 205),
          rgb(195, 129, 212));
      z-index: 9;
    }

    .modal-dialog {
      left: 0 !important;
      top: 25% !important;
    }

    #cv-title {
      cursor: pointer;
      border: 1px solid grey;
    }
  </style>

</head>

<body>

  <?php

  $conn = new mysqli("localhost:3307", "root", "Lisaco563", "bitnami_df");
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  $userName = $_SESSION["username"];
  $sql = "SELECT * FROM `users` WHERE username = '$userName'";

  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
      $skills = $row['skills'];
      $isClient = ($skills == 'Recruiter') ? true : false;
      if ($isClient) {
        $tabs = '<a href="/applicant_list.php">Applied Jobs</a><a href="/shortlisted_list.php">Shortlisted</a>';
      } else {
        $tabs = '<a href="/jobs_on_applied.php">Applied Jobs</a>';
      }
      echo '
    
    <div id="mySidenav" class="sidenav gradient-bg">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"
        >&times;</a
      >
      <a href="/index.php">Home</a>
      <a href="/myvideos.php">My Videos</a>
      ' . $tabs . '
      <a href="/logout.php">Logout</a>
    </div>

    <div class="container-fluid" style="z-index:11;position:fixed;top:0;background-color:#fff">
      <div class="row pt-3">
        <div class="col-lg-3">
          <div class="logoo">
            <span style="font-size: 30px; cursor: pointer;" onclick="openNav()"
              >&#9776;</span
            >
            <a href="index.php"><img src="simx.png" /></a>
          </div>
          <span id="mobile-logout">
          <span class="pr-3" style="font-weight:bold;">' . $row["name"] . '</span>
          <img
            src="http://www.simx.tv/picture/Photos/' . $row["username"] . '.png"
            alt="Avatar"
            class="avatar"
          />
          </span>
        </div>
        <div class="col-lg-5 pt-2">
          <form class="navbar-form" action="search.php" method="post">
            <div class="input-group">
              <input
                class="form-control"
                type="text"
                name="name"
                placeholder="Search"
              />
              <span class="input-group-btn">
                <button type="submit" name="submit" value="submit" class="btn btn-default">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </span>
            </div>
          </form>
        </div>
        <div id="web-logout" style="text-align: right;" class="col-lg-4 profile-img">
        <span class="pr-3" style="font-weight:bold;">' . $row["name"] . '</span>
          <img
            src="http://www.simx.tv/picture/Photos/' . $row["username"] . '.png"
            alt="Avatar"
            class="avatar"
          />
          <a
            style="background-color: #005585;"
            type="button"
            class="btn btn-info"
            href="logout.php"
          >
            LogOut
          </a>
        </div>
      </div>
    </div>
    ';
    }
  } else {
    echo "0 results";
  }

  ?>
  <h1 style="height: 20px;"></h1>
  <div class="container-fluid mt-5">

    <div class="row pt-5">

      <?php
      $conn = new mysqli("localhost:3307", "root", "Lisaco563", "bitnami_df");
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }
      $broadcastId = $_GET['id'];

      // $sqlo = "SELECT t.tag  FROM `tags` AS t WHERE  t.tag = '$broadcastId'";
      $sql = "SELECT b.*, u.username AS uploader_pic FROM `broadcasts` AS b, `users` AS u WHERE u.username = b.username AND broadcast = '$broadcastId'";

      $result = $conn->query($sql);

      if ($result->num_rows > 0) {
        $rows = $result->fetch_assoc();




        if ($rows['isOffline']) {
          $src = "https://simx.s3-us-west-2.amazonaws.com/offlineVideos/$rows[broadcast].mp4";
        } else {
          $src = "https://simx.s3-us-west-2.amazonaws.com/recordedvideos/$rows[broadcast].mp4";
        }
      ?>
        <div class="col-lg-8 col-md-12 col-sm-12" id="left-div-player">
          <?php

          $bid = $rows['id'];

          $b = $rows['broadcast'];

          $un = $_SESSION["username"];

          ?>
          <?php echo '
                <video id="main-video-js" class="video-js" controls autoplay loop>
                    <source src="' . $src . '" type="video/mp4" />
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider
                        upgrading to a web browser that
                        <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
                ';
          ?>
          <div style="padding-left:1%; padding-top:2%;">

            <?php
            $conn = new mysqli("localhost:3307", "root", "Lisaco563", "bitnami_df");
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }
            $broadcastId = $_GET['id'];
            $sqlo = "SELECT * FROM `jobcandidates` AS t WHERE  t.broadcast ='$broadcastId' and t.username = '$userName'";
            $results = $conn->query($sqlo);
            $applied = false;
            if ($results->num_rows > 0) {
              $applied = true;
            }
            $sqlo = "SELECT DISTINCT t.tag AS tag FROM `tags` AS t WHERE  t.broadcast ='$broadcastId'";
            $results = $conn->query($sqlo);

            if ($results->num_rows > 0) {
              while ($row = $results->fetch_assoc()) {
                echo '<span class="h-5" style="color:blue;"> #' . $row["tag"] . '</span>';
              }
            }
            ?>

          </div>
          <?php

          $query = "SELECT v.title as title, v.videocv as cv, v.id as id FROM `videocvs` as v WHERE v.username = '$userName'";

          $result = $conn->query($query);

          if ($result->num_rows > 0) {

          ?>
            <!-- Modal -->
            <div class="modal mt-4 fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Select Your Video CV</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <?php
                    while ($row = $result->fetch_assoc()) { // echo "<pre> $query"; print_r($row);
                    ?>
                      <div id="cv-div" class="card-body">
                        <div class="row" onclick="apply('#sendCv-<?php echo $row['id']; ?>','<?php echo $row['id']; ?>','<?php echo $un; ?>', '<?php echo $b; ?>' , '<?php echo $bid; ?>')">
                          <div class="col-12">
                            <h5 class="card-title h3"><?php echo $row['title']; ?></h5>
                          </div>
                        </div>
                      </div>
                    <?php } ?>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                  </div>
                </div>
              </div>
            </div>
          <?php
          } ?>


          <div class="row">
            <div class="col-8">
              <?php echo ' <h3 class="mt-1">' . $rows["title"] . '</h3>' ?>
            </div>
            <div style="padding-left:20%;    padding-top: 0%;" class="col-4 send-btn-for-upload ">
            </div>
          </div>
          <div class="row text-muted">
            <?php echo '<div class="col-12"> ' . $rows["viewers"] . ' views . Upload Date: ' . explode(" ", $rows["time"])[0] . '
                    </div>' ?>
          </div>
          <div class="row mt-3">
            <?php echo '
                    <div class="float-left">
                        <img src="http://www.simx.tv/picture/Photos/' . $rows["uploader_pic"] . '.png" alt="Avatar"
                        class="avatar" /> ' ?>
          </div>
          <div class="ml-3">
            <?php echo '<p class="pt-2" style="font-weight: bold; font-size: 15px;">' . $rows["name"] . '</p>' ?>
          </div>
        </div>
        <?php
        if ($rows["isJob"] == 1 && $skills == 'OpenForWork') {
          if ($applied) {
          } else {
        ?>
            <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
              Apply with Video
            </button>


        <?php
          }
        } ?>
    </div>

  <?php
      } else {
        echo "0 results";
      }

  ?>

  <div class="col-lg-1 col-md-12 col-sm-12"></div>
  <div class="col-lg-3 col-md-12 col-sm-12">
    <?php
    $conn = new mysqli("localhost:3307", "root", "Lisaco563", "bitnami_df");
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }
    $broadcastId = $_GET['id'];
    $sql = "SELECT * FROM `broadcasts` WHERE broadcast != '$broadcastId' LIMIT 10 ";
    // $sql = "SELECT b.*, u.username AS uploader, u.picture AS uploader_pic FROM `broadcasts` AS b, `users` AS u WHERE u.username = b.username";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
      // output data of each row
      while ($row = $result->fetch_assoc()) {
        echo '
        <a style="text-decoration: none;  color: black;" href="broadcast.php?id=' . $row["imglink"] . '">
      <div class="row mt-5 ">
        <div class="col-5">
          <div style="background-color: white; height:140px;">
            <img class="thambnail"
            src="http://www.simx.tv/picture/Photos/' . $row["imglink"] . '.png" />
          </div>
        </div>
        <div class="col-7 pl-0">
          <div class="row">
            <div class="col-12">
              <p style="font-weight: bold; font-size: 18px;">
                ' . $row["title"] . '
              </p>
            </div>
          </div>
          <div class="row">
            <div class="col-12 pt-1 text-muted">
              <p>
                ' . $row["name"] . ' <br />
                ' . $row["viewers"] . ' Views . Upload Date: ' . explode(
          " ",
          $row["time"]
        )[0] . '
              </p>
            </div>
          </div>
        </div>
      </div>
      </a>
    
      ';
      }
    } else {
      echo "0 results";
    }

    ?>
  </div>

  <script>
    function openNav() {
      document.getElementById("mySidenav").style.width = "250px";
    }

    function closeNav() {
      document.getElementById("mySidenav").style.width = "0";
    }
  </script>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script>
    function apply(id, cvid, username, broadcast, bid) {
      $(".modal-dialog").fadeOut('slow');

      // $(id).html('<div><img src="30.gif" /></div > ');
      $.post("applyForJob.php", {
          username: username,
          broadcast: broadcast,
          broadcastId: bid,
          cvid: cvid
        },
        function(data, status) {
          // $(id).html(
          //   '<span><span class="glyphicon glyphicon-send"></span> &nbsp; &nbsp;Apply With Video</span>'
          // );
          console.log("Data: " + data + "\nStatus: " + status);
          var myObj = JSON.parse(data);
          if (status == "success") {
            alert(myObj.message)
          } else {
            alert("somting went wrong")
          }
          location.reload();

        });
    }
  </script>
</body>

</html>